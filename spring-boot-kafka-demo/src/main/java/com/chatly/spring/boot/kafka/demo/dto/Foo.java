package com.chatly.spring.boot.kafka.demo.dto;

/**
 * @author lihuazeng
 */
public class Foo {
    private String name;
    private String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Foo{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
