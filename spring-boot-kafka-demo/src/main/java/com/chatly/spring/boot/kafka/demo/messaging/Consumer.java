package com.chatly.spring.boot.kafka.demo.messaging;

import com.chatly.spring.boot.kafka.demo.dto.Foo;
import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * @author lihuazeng
 */
@Component
@KafkaListener(id = "multiGroup", topics = { "spring_boot_kafka_demo_topic"})
public class Consumer {

    @KafkaHandler
    public void foo(Foo foo) {
        System.out.println("Received: " + foo);
    }
}
