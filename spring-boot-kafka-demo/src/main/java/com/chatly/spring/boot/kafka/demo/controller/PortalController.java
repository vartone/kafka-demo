package com.chatly.spring.boot.kafka.demo.controller;

import com.chatly.spring.boot.kafka.demo.dto.Foo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;

/**
 * @author lihuazeng
 */
@RestController
public class PortalController {
    @Autowired(required = false)
    private KafkaTemplate<String, Object> kafkaTemplate;

    @GetMapping(path = "/send/{name}")
    public void sendFoo(@PathVariable String name) {
        Foo foo = new Foo();
        foo.setName(name);
        foo.setType("foo");
        ListenableFuture<SendResult<String, Object>> future = this.kafkaTemplate.send("spring_boot_kafka_demo_topic", foo);
        try {
            SendResult<String, Object> sendResult = future.get();
            System.out.println(sendResult.toString());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
