package com.chatly.spring.cloud.stream.kafka.demo.controller;

import com.chatly.spring.cloud.stream.kafka.demo.messaging.receiver.CallbackEventMsg;
import com.chatly.spring.cloud.stream.kafka.demo.messaging.source.CallbackEventSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lihuazeng
 */
@RestController
@EnableBinding(CallbackEventSource.class)
public class PortalController {

    @Autowired
    private CallbackEventSource callbackEventSource;

    @GetMapping(path = "/send/{json}")
    public void sendMsg(@PathVariable String json) {
        CallbackEventMsg msg = new CallbackEventMsg("accountId", "botAccountId", json);
        boolean r = callbackEventSource.output().send(MessageBuilder.withPayload(msg).build());
        System.out.println("callback msg send to mq response: "+ r);
    }
}
