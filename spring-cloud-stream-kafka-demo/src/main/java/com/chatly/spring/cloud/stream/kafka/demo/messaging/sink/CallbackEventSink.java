package com.chatly.spring.cloud.stream.kafka.demo.messaging.sink;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

/**
 * @author lihuazeng
 */
public interface CallbackEventSink {
    /**
     * input channel
     */
    String CHANNEL_BROADCAST = "input-channel-demo";

    @Input(CallbackEventSink.CHANNEL_BROADCAST)
    SubscribableChannel input();
}
