package com.chatly.spring.cloud.stream.kafka.demo.messaging.source;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/**
 * @author lihuazeng
 */
public interface CallbackEventSource {
    /**
     *  output channel
     */
    String CHANNEL_BROADCAST = "output-channel-demo";

    /**
     * @return
     */
    @Output(CallbackEventSource.CHANNEL_BROADCAST)
    MessageChannel output();
}
