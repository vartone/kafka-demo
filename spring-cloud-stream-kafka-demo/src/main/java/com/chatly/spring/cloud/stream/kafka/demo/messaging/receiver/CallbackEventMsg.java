package com.chatly.spring.cloud.stream.kafka.demo.messaging.receiver;

import java.io.Serializable;

/**
 * @author lihuazeng
 */
public class CallbackEventMsg implements Serializable {
    private String accountId;
    private String botAccountId;
    private String message;

    public CallbackEventMsg() {
    }

    public CallbackEventMsg(String accountId, String botAccountId, String message) {
        this.accountId = accountId;
        this.botAccountId = botAccountId;
        this.message = message;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getBotAccountId() {
        return botAccountId;
    }

    public void setBotAccountId(String botAccountId) {
        this.botAccountId = botAccountId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "CallbackEventMsg{" +
                "accountId='" + accountId + '\'' +
                ", botAccountId='" + botAccountId + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
