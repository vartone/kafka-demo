package com.chatly.spring.cloud.stream.kafka.demo.messaging.receiver;

import com.chatly.spring.cloud.stream.kafka.demo.messaging.sink.CallbackEventSink;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;

/**
 * @author lihuazeng
 */
@EnableBinding(CallbackEventSink.class)
public class CallbackEventReceiver {


    @StreamListener(value = CallbackEventSink.CHANNEL_BROADCAST)
    public void receive(Message<CallbackEventMsg> message) {
        CallbackEventMsg msg = message.getPayload();
        System.out.println("receive msg:"+msg);
        Acknowledgment acknowledgment = message.getHeaders().get(KafkaHeaders.ACKNOWLEDGMENT, Acknowledgment.class);
        if (acknowledgment != null) {
            acknowledgment.acknowledge();
            System.out.println("acknowledgment provided message:"+msg);
        }
    }
}
